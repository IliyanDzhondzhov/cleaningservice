package uk.tedAndGeorge.cleaningService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TedAndGeorgeCleaningServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TedAndGeorgeCleaningServiceApplication.class, args);
	}

}
