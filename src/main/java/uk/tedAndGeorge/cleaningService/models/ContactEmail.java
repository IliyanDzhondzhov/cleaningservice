package uk.tedAndGeorge.cleaningService.models;

import org.springframework.web.multipart.MultipartFile;
import uk.tedAndGeorge.cleaningService.validation.ContactFileConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ContactEmail {
    @NotNull
    @Size(min = 2, max = 100)
    private String clientFirstName;

    @NotNull
    @Size(min = 2, max = 100)
    private String clientLastName;

    @NotNull
    @Pattern(regexp=".+@.+\\..+", message="Your Email must contain: @[domain].com/net/org etc.")
    private String clientEmail;

    @NotNull
    @Size(min = 10, max = 500)
    private String emailText;

    @ContactFileConstraint
    private MultipartFile[] attachments;

    public ContactEmail(){

    }

    public ContactEmail(String clientFirstName, String clientLastName, String clientEmail, String emailText, MultipartFile[] attachments) {
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientEmail = clientEmail;
        this.emailText = emailText;
        this.attachments = attachments;
    }

    public MultipartFile[] getAttachments() {
        return this.attachments;
    }

    public String getClientFirstName() {
        return this.clientFirstName;
    }

    public String getClientLastName() {
        return this.clientLastName;
    }

    public String getClientEmail() {
        return this.clientEmail;
    }

    public String getEmailText() {
        return this.emailText;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    public void setAttachments(MultipartFile[] attachments) {
        this.attachments = attachments;
    }
}