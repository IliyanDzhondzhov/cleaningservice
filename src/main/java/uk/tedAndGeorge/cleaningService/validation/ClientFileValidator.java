package uk.tedAndGeorge.cleaningService.validation;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ClientFileValidator implements
        ConstraintValidator<ContactFileConstraint, MultipartFile[]> {
    private static final long VALID_FILE_SIZE = 5242880;

    @Override
    public void initialize(ContactFileConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(MultipartFile[] multipartFiles, ConstraintValidatorContext constraintValidatorContext) {
        return isValidLength(multipartFiles) && isValidSize(multipartFiles) && isValidType(multipartFiles);
    }

    private boolean isValidLength(MultipartFile[] multipartFiles) {
        return multipartFiles.length <= 5;
    }

    private boolean isValidSize(MultipartFile[] multipartFiles) {
        for (int file = 0; file < multipartFiles.length; file++) {
            if (multipartFiles[file].getSize() > VALID_FILE_SIZE) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidType(MultipartFile[] multipartFiles) {
        for (int file = 0; file < multipartFiles.length; file++) {
            if (!"image/png".equals(multipartFiles[file].getContentType())
                    && !"image/jpeg".equals(multipartFiles[file].getContentType())
                    && !"".equals(multipartFiles[file].getOriginalFilename())) {
                return false;
            }
        }
        return true;
    }

}
