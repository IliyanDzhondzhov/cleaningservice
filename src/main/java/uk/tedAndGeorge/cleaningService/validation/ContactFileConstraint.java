package uk.tedAndGeorge.cleaningService.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ClientFileValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ContactFileConstraint {
    String message() default "Invalid attachment format or size too big, files must be one the following types : .png, .jpeg, .jpg or .jpe and files must be maximum 5mb";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
