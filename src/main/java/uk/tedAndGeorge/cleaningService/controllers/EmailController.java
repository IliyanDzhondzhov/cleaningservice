package uk.tedAndGeorge.cleaningService.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import uk.tedAndGeorge.cleaningService.models.ContactEmail;
import uk.tedAndGeorge.cleaningService.services.EmailService;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class EmailController {

    private static final Logger logger = LogManager.getLogger(EmailController.class);
    private EmailService emailService;

    @Autowired
    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @RequestMapping(value = "/sendHTMLMail", method = POST)
    public String sendHTMLMail(@Valid ContactEmail contactEmail, BindingResult bindingResult, final Locale locale) throws MessagingException, IOException {

        if (bindingResult.hasErrors()) {
            return "redirect:/";
        }
        this.emailService.sendHTMLMail(contactEmail);
        this.emailService.sendHTMLMailToClient(contactEmail);
        return "redirect:/";
    }



}
