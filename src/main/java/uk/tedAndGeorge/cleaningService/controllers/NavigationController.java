package uk.tedAndGeorge.cleaningService.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import uk.tedAndGeorge.cleaningService.models.ContactEmail;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class NavigationController {
    @RequestMapping(value = "/", method = GET)
    public String navigateHomePage(ContactEmail contactEmail){
        return "index.html";
    }
    @RequestMapping(value = "/termsAndConditions", method = GET)
    public String navigateToTermsAndConditions(){
        return"terms.html";
    }
    @RequestMapping(value = "/privacyPolicy", method = GET)
    public String navigateToPrivacyPolicy(){
        return "privacy.html";
    }

}
