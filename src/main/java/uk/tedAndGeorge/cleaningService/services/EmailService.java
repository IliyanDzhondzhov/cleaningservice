package uk.tedAndGeorge.cleaningService.services;

import uk.tedAndGeorge.cleaningService.models.ContactEmail;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Locale;

public interface EmailService {

    void sendHTMLMail(ContactEmail contactEmail) throws MessagingException, IOException;
    void sendHTMLMailToClient(ContactEmail contactEmail) throws MessagingException;
}
