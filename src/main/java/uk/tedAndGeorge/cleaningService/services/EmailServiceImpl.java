package uk.tedAndGeorge.cleaningService.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uk.tedAndGeorge.cleaningService.models.ContactEmail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Component
public class EmailServiceImpl implements EmailService {
    private static final Logger logger = LogManager.getLogger(EmailServiceImpl.class);
    private static final String EMAIL_ADMIN_TEMPLATE_NAME = "emailTemplates/adminEmail";
    private static final String EMAIL_CLIENT_TEMPLATE_NAME = "emailTemplates/clientEmail";

    private JavaMailSenderImpl mailSender;
    private TemplateEngine htmlTemplateEngine;

    @Autowired
    public EmailServiceImpl(JavaMailSenderImpl mailSender, TemplateEngine htmlTemplateEngine) {
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
    }


    public  static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
        multipart.transferTo(convFile);
        return convFile;
    }

    @Override
    public void sendHTMLMail(ContactEmail contactEmail) throws MessagingException, IOException {
        final Context ctx = new Context();
        ctx.setVariable("name", contactEmail.getClientFirstName());
        ctx.setVariable("lastName", contactEmail.getClientLastName());
        ctx.setVariable("clientEmail", contactEmail.getClientEmail());
        ctx.setVariable("emailText", contactEmail.getEmailText());

        final MimeMessage message = this.mailSender.createMimeMessage();
        message.setSubject("no-reply");
        message.setFrom("tgcleaningsolutions@gmail.com");
        message.addRecipients(Message.RecipientType.TO, "tgcleaningsolutions@gmail.com");

        Multipart multipart = new MimeMultipart();

        MimeBodyPart textPart = new MimeBodyPart();

        MimeBodyPart attachmentPart;

        for (MultipartFile attachment : contactEmail.getAttachments()) {
            if(!Objects.requireNonNull(attachment.getOriginalFilename()).isEmpty()){
                attachmentPart = new MimeBodyPart();
                attachmentPart.attachFile(multipartToFile(attachment, attachment.getOriginalFilename()));
                multipart.addBodyPart(attachmentPart);
            }
        }

        final String htmlContent = this.htmlTemplateEngine.process(EMAIL_ADMIN_TEMPLATE_NAME, ctx);
        textPart.setContent(htmlContent, "text/html; charset=utf-8");
        multipart.addBodyPart(textPart);
        message.setContent(multipart);

        this.mailSender.send(message);
    }

    @Override
    public void sendHTMLMailToClient(ContactEmail contactEmail) throws MessagingException {
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
        message.setSubject("no-reply");
        message.setFrom("tgcleaningsolutions@gmail.com");
        message.setTo(contactEmail.getClientEmail());
        Context ctx = new Context();
        final String htmlContent = this.htmlTemplateEngine.process(EMAIL_CLIENT_TEMPLATE_NAME, ctx);
        message.setText(htmlContent, true);

        this.mailSender.send(mimeMessage);

    }
}
