package uk.tedAndGeorge.cleaningService.services;

public interface ContactEmailProcessor {
    void processContactEmail();
}
